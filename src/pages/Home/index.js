import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
} from 'react-native';
import {ImageHeader} from '../../assets';
import {Saldo, Promo} from '../../components/';
import ButtonIcon from '../../components/ButtonIcon';
import {WARNA_ABU_ABU} from '../../utils/constant';
import {ScrollView} from 'react-native-gesture-handler';

const Home = () => {
  return (
    <View style={styles.page}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <ImageBackground source={ImageHeader} style={styles.header}>
          <View style={styles.kelompok5}>
            <Text style={styles.ovocash}>OVO Cash, </Text>
            <Text style={styles.saldo}>Rp 200.000.000</Text>
            <Text style={styles.ovopoint}>OVO Point 1.000.000</Text>
            
          </View>
        </ImageBackground>
        <Saldo />
          <View style={styles.layanan}>
          <Text style={styles.label}>Topup Lainya</Text>
          <View style={styles.iconLayanan}>
            <ButtonIcon title="PLN" type="layanan" />
            <ButtonIcon title="Pulsa" type="layanan" />
            <ButtonIcon title="Paket Data" type="layanan" />
            <ButtonIcon title="Pasca bayar" type="layanan" />
            </View>
            </View>
          <View style={styles.layanan}>
          <View style={styles.iconLayanan}>
            <ButtonIcon title="BPJS" type="layanan" />
            <ButtonIcon title="TV" type="layanan" />
            <ButtonIcon title="Lingkungan" type="layanan" />
            <ButtonIcon title="Lainnya" type="layanan" />
            </View>
            </View>
        <View style={styles.Promo}>
          <Text style={styles.label}>Info dan Promo lainnya</Text>
          <Promo title="Ada yang baru nih " status="Lihat"/>
          <Promo title="OVO ada Promo Seru" status="Buruan!!!"/>
          <Promo title="Jangan sampai kelewat !!" status="Lihat"/>
          <Promo title="Ada yang seru juga nih" status="Lihat"/>
        </View>
      </ScrollView>
    </View>
  );
};

export default Home;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.3,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  kelompok5: {
    marginTop: windowHeight * 0.09,
  },
  ovocash: {
    fontSize: 24,
    fontFamily: 'TitilliumWeb-Regular',
    color: 'white'
  },
  saldo: {
    fontSize: 24,
    fontFamily: 'TitilliumWeb-Bold',
    color: 'white'
  },
  ovopoint: {
    fontSize: 15,
    fontFamily: 'TitilliumWeb-Bold',
    color: 'white'
  },
  layanan: {
    paddingLeft: 30,
    paddingTop: 15,
  },
  label: {
    fontSize: 18,
    fontFamily: 'TitilliumWeb-Bold',
    color: 'white'
  },
  iconLayanan: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 9,
    marginLeft: -13,
    marginRight: 9,
    marginStart: 1,
    flexWrap: 'nowrap',
    alignItems: 'center',
  },
    Promo: {
    paddingTop: 10,
    paddingHorizontal: 30,
    backgroundColor: WARNA_ABU_ABU,
    flex: 1,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
});
