import Home from './Home'
import Profile from './Profile'
import Scan from './Scan'
import Finance from './Finance'
import Deals from './Deals'
import Kelompok5 from './Kelompok5'


export { Kelompok5, Profile, Home, Scan, Finance, Deals }